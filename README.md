[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![DOI:3384943.3409428](http://img.shields.io/badge/DOI-3384943.3409428-blue.svg)](https://dl.acm.org/doi/abs/10.1145/3384943.3409428)

# SmartWitness: A Proactive Software Transparency System using Smart Contracts

Over the last years, multiple cyberattacks took advantage of several flaws
presented in software distribution systems. Attackers have been able to
deploy malicious software on thousands of computers around the world using
software distribution platforms from trusted IT providers. Most recently,
one attack managed to compromise government agencies and well-known 
corporations in the United States. This proves that current cybersecurity
approaches are unsatisfactory to detect and stop these stealthy attacks
effectively.

In this paper, we present a design for software distribution systems based
on permissionless public ledgers named SmartWitness. We propose
a novel distribution system that uses smart contracts instead of
traditional code signing certificates. This change enables us to provide
cybersecurity properties of binary transparency, useful and granular 
software revocation, and dynamic and proactive security assessment improving
risk awareness of software users. We develop a proof of concept by integrating
our solution into existing package managers from different operating 
systems. We also present empirical metrics taken from conducted experiments
indicating that our design is practical, feasible and deployable.

## Repository Structure

- [/aptserver](/aptserver) - prototype scripts to modify and manage APT repositories and integrate SmartWitness
- [/aptclient](/aptclient) - prototype scripts to install package from APT repositories using SmartWitness
- [/fdroidserver](/fdroidserver) - F-Droid Sever [v1.11](), changed to integrate SmartWitness
- [/fdroidclient](/fdroidclient) - F-Droid Client [v1.11](), changed to integrate SmartWitness

## Citation
If you find our work useful in your research, consider citing our paper:

```bibtex
@inproceedings{guarnizo2020smartwitness,
  title={SmartWitness: A Proactive Software Transparency System using Smart Contracts},
  author={Guarnizo, Juan and Alangot, Bithin and Szalachowski, Pawel},
  booktitle={Proceedings of the 2nd ACM International Symposium on Blockchain and Secure Critical Infrastructure},
  pages={117--129},
  year={2020}
}
```

## License
The SmartWitness prototype and related artifacts are licensed under the terms of the [Apache 2.0](LICENSE).
