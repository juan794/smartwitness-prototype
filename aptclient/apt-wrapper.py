#!/usr/bin/env python3

import io
import re
import yaml
import time
import ntpath
import argparse
import subprocess
import eth_utils as utils

from web3 import Web3

CONFIG_FILE = "config.yml"

w3 = None
cgf = None
config = {}
packages = None

def main():
    global w3
    global cfg
    global config

    with open(CONFIG_FILE, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    w3 = Web3(Web3.HTTPProvider(cfg['rpc-host']))

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--install", help="action to execute: install or audit")
    parser.add_argument("-l", "--list", action='store_true', help="list all packages and their information")
    parser.add_argument("-u", "--update", action='store_true', help="list all packages and their information")
    parser.add_argument("-d", "--depends", help="dependencies to validate")
    parser.add_argument("--min-score", help="Minimum security score value (0 by default)", default=0, type=int)
    parser.add_argument("--min-votes", help="Minimum security score votes (0 by default)", default=0, type=int)
    parser.add_argument("--min-freshness", help="Minimum difference of proof freshness compared to local known blocks (100 blocks by default)", default=1000, type=int)
    parser.add_argument("--only-last-version", action='store_true', help="Only install last versions of packages")
    parser.add_argument("--verbose", action='store_true', help="Print details for every package")
    parser.add_argument("--y", action='store_true', help="Do not ask for confirmation")

    args = parser.parse_args()

    config['min-score'] = args.min_score
    config['min-votes'] = args.min_votes
    config['min-freshness'] = args.min_score
    config['only-last-version'] = True if args.only_last_version else False
    config['confirm'] = True if not args.y else False
    config['verbose'] = True if args.verbose else False

    if args.install:
        process_installation(args.install, args.depends)
    elif args.list:
        list_all_available_pkgs()
    elif args.update:
        update_package_list()

def list_all_available_pkgs():
    process_out = subprocess.check_output(["dpkg-query", "-W"])

    buf = io.StringIO(process_out.decode())
    for line in buf.readlines():
        line = line.replace("\n", "").split("\t")

        pkg = line[0]
        version = line[1]

        (pkg_hash,
         dev_pk,
         ca,
         dev_addr,
         witness_addr,
         proof_block,
         latest_version,
         security_score,
         number_votes,
         expiration) = extract_package_info(pkg)

        print(pkg, version, security_score, number_votes)

def process_installation(target_package, depends_packages):
    global config
    global packages

    packages = []
    get_all_dependencies(depends_packages)
    if (len(packages) >= 1):
        print("Additional packages required. We are going to install the following packages:")
        print(', '.join(packages))
        decision = "n"
        if not config["confirm"]:
            decision = "y"
        else:
            decision = input("Do you want to continue? [Y/n] ")
        if not decision or decision == "Y" or decision == "y":
            try:
                for package in packages:
                    validate_package(package)

                #### EVERYTHING OK ####
                run_apt_install(target_package)
            except Exception as e:
                raise

def update_package_list():
    process = subprocess.check_output(["sudo", "apt-get", "update"])
    print(process.decode())

def run_apt_install(pkg):
    #### NEED TO CONFIGURE sudo TO NOPASSWD ####
    process = subprocess.check_output(["sudo", "apt-get", "install", "-y", pkg])
    #process = subprocess.check_output(["sudo", "apt-get", "-y", "dist-upgrade"])
    print(process.decode())

def validate_package(package):
    try:
        (pkg_hash,
         dev_pk,
         ca,
         dev_addr,
         witness_addr,
         proof_block,
         latest_version,
         security_score,
         number_votes,
         expiration) = extract_package_info(package)

        process_package_info(package,
                             pkg_hash,
                             proof_block,
                             latest_version,
                             security_score,
                             number_votes,
                             expiration,
                             ca)
    except Exception as e:
        raise

def process_package_info(package, pkg_hash, proof_block, latest_version, security_score, number_votes, expiration, ca):
    global w3
    global cfg
    global config

    current_block = w3.eth.getBlock('latest')
    current_time = time.time()

    block_difference = current_block['number'] - proof_block

    if config['verbose']:
        print("[Info] %s security score: %i" % (package, security_score))
        print("[Info] %s number of votes: %i" % (package, number_votes))
        print("[Info] %s proof block difference: %i" % (package, block_difference))

    if block_difference > config['min-freshness']:
        print("[Error] %s proof is not fressh: %i" % (package, block_difference))
        raise
    if current_time >= expiration:
        print("[Error] witness contract for %s is expired" % package)
        raise
    if config['only-last-version'] and pkg_hash != latest_version:
        print("[Error] Available version of %s is not the latest" % package)
        raise
    if config['min-score'] > security_score:
        print("[Error] Security score of %s lower than minimum: %i" % (package, security_score))
        raise
    if config['min-votes'] > number_votes:
        print("[Error] Number of votes for %s lower than minimum: %i" % (package, number_votes))
        raise
    if ca not in cfg['trusted-ca']:
        print("[Error] Witness contract issuer for %s is not trusted" % package)
        raise

    print("[OK] %s" % package)

def extract_package_info(pkg):
    global cfg


    json_uri = None
    h = None
    dev_pk = None
    ca = None
    ca_nonce = None
    dev_addr = None
    witness_addr = None
    try:
        info = subprocess.check_output(["apt-cache", "show", pkg])
        buf = io.StringIO(info.decode())

        for line in buf.readlines():
            if line.startswith("SHA256:"):
                h = line.replace("SHA256:", "") \
                        .replace("\n", "") \
                        .strip()
                h = '0x' + h
            if line.startswith("CA:"):
                ca = line.replace("CA:", "") \
                        .replace("\n", "") \
                        .strip()
            if line.startswith("CA-Nonce:"):
                ca_nonce = line.replace("CA-Nonce:", "") \
                            .replace("\n", "") \
                            .strip()
            if line.startswith("Filename:"):
                uri = line.replace("Filename:", "") \
                          .replace("\n", "") \
                          .strip()
                json_uri = uri.replace(".deb", ".json")
                json_uri = json_uri.replace(".DEB", ".json")
            if line.startswith("Dev-PK:"):
                dev_pk = line.replace("Dev-PK:", "") \
                            .replace("\n", "") \
                            .strip()

        info = subprocess.check_output(["apt-cache", "policy", pkg])
        url = re.search("(?P<url>https?://[^\s]+)", info.decode()).group("url")

        dev_addr = from_public_to_address(dev_pk)
        witness_addr = checksum_address_from(ca, int(ca_nonce))

        output = subprocess.check_output(["node", 
                                          "trie-validator.js", pkg,
                                                               url,
                                                               json_uri,
                                                               h,
                                                               dev_addr,
                                                               witness_addr,
                                                               cfg['rpc-host']])
        # [0] block-number, [1] expiration date, [2] hash last version, [3] security score and votes counter
        nodejs_return = output.decode().replace("\n", "").split(" ")
        proof_block = int(nodejs_return[0].replace("0x", ""), 16)
        latest_version = nodejs_return[2]
        security_score = int(nodejs_return[3][-2:], 16)
        number_votes = int(nodejs_return[3].replace("0x", "")[0:-2], 16)
        expiration = int(nodejs_return[1].replace("0x", ""), 16)

        return (h,
                dev_pk,
                ca,
                dev_addr,
                witness_addr,
                proof_block,
                latest_version,
                security_score,
                number_votes,
                expiration)
    except Exception as e:
        raise

def get_all_dependencies(pkgs):
    global packages

    all_pkgs = pkgs.split(" ")

    for pkg in all_pkgs:
        #info = subprocess.check_output(["apt-cache", "show", pkg])
        #buf = io.StringIO(info.decode())

        #if (len(info) <= 0) or ("N: " in info.decode()):
        #    continue

        packages.append(pkg)
        '''
        for line in buf.readlines():
            if line.startswith("Depends:") or line.startswith("Recommends:"):
                line = line.replace("Depends:", "") \
                        .replace("Recommends:", "") \
                        .replace("\n", "")
                depends = re.sub(r"\(.[^\)]+\)", "", line).split(",")
                for dependency in depends:
                    if "|" in dependency:
                        dependency = dependency.split("|")[0].strip()
                    dependency = dependency.strip()
                    if dependency in packages:
                        continue

                    #### INSTALLED CHECKER ####
                    try:
                        output = subprocess.check_output(["apt-cache", "policy", dependency])
                        if "Installed: (none)" not in output.decode():
                            continue
                    except Exception as e:
                        raise
                    #### END ####

                    #get_all_dependencies(dependency)
        '''
    print("Finish dependencies")                        

def from_public_to_address(public_key):
    global w3

    if not public_key.startswith("0x"):
        public_key = "0x" + public_key
    h = w3.sha3(hexstr=public_key)[-20:].hex()
    address = w3.toChecksumAddress(h)
    address = "0x" + address.replace("0x", "").lstrip("0")
    return address

def checksum_address_from(_origin, _nonce):
    _origin = bytes.fromhex(_origin.replace('0x', ''))
    if _nonce == 0x00:
        data = b''.join([b'\xd6', b'\x94', _origin, b'\x80'])
    elif _nonce <= 0x7f:
        data = b''.join([b'\xd6', b'\x94', _origin, bytes([_nonce])])
    elif _nonce <= 0xff:
        data = b''.join([b'\xd7', b'\x94', _origin, b'\x81', bytes([_nonce])])
    elif _nonce <= 0xffff:
        data = b''.join([b'\xd8', b'\x94', _origin, b'\x82', bytes([_nonce])])
    elif _nonce <= 0xffffff:
        data = b''.join([b'\xd9', b'\x94', _origin, b'\x83', bytes([_nonce])])
    else:
        data = b''.join([b'\xda', b'\x94', _origin, b'\x84', bytes([_nonce])])
    addr = '0x' + utils.keccak(data)[-20:].hex()
    return utils.to_checksum_address(addr)

def get_filename(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

if __name__ == '__main__':
    main()
