#!/usr/bin/env node

const BN = require('bn.js');
const request = require('request');
const util = require('ethereumjs-util');
const Trie = require('merkle-patricia-tree');
const { exec } = require('child_process');

const RPC_SERVER = "http://localhost:8545";

name = process.argv[2];
url = process.argv[3];
json_uri = process.argv[4];
pkg_hash = process.argv[5];
dev_addr = process.argv[6];
witness_addr = process.argv[7];

request({uri:(url + "/" + json_uri), method:'GET', headers: {'Content-Type':'application/json'}}, (err, resp, body) => {
    if(resp.statusCode == 200) {
        proof = JSON.parse(body);
        if (proof.balance == '0x0') {proof.balance = ''}
        request({uri: RPC_SERVER, method:'POST', json:{jsonrpc:'2.0', method:'eth_getBlockByNumber', params:[proof['block-number'], false], id:67}, headers: {'Content-Type':'application/json'}}, (err, resp, body) => {
            stateRoot = body['result']['stateRoot'];
            Trie.verifyProof(stateRoot, util.keccak256(witness_addr), proof.accountProof, (err, value) => {
                if (err || !value.equals(util.rlp.encode([proof.nonce, 
                                                          proof.balance, 
                                                          proof.storageHash, 
                                                          proof.codeHash]))){
                    throw new Error('Error validating witness proof');
                }
            });
            // VALIDATING DEVELOPER OWNERSHIP OVER WITNESS CONTRACT
            Trie.verifyProof(proof.storageHash, util.keccak256('0x0000000000000000000000000000000000000000000000000000000000000000'), proof.storageProof[0].proof, (err, value) => {
                if (err || !value.equals(util.rlp.encode(dev_addr))){
                    throw new Error('Error validating ownership proof');
                }
            });
            // VALIDATING WITNESS CONTRACT EXPIRATION
            Trie.verifyProof(proof.storageHash, util.keccak256('0x0000000000000000000000000000000000000000000000000000000000000001'), proof.storageProof[1].proof, (err, value) => {
                if (err || !value.equals(util.rlp.encode(proof.storageProof[1].value))){
                    throw new Error('Error validating expiration proof');
                }
            });
            // VALIDATING PACKAGE REGISTRATION
            key_last_version = util.keccak256(util.bufferToHex(util.toBuffer(name)) + '0000000000000000000000000000000000000000000000000000000000000002');
            n = new BN(util.bufferToHex(key_last_version).replace('0x', ''), 16);
            n = n.add(new BN(1));
            key_score = util.keccak256(pkg_hash + n.toString(16).padStart(64, '0'));
            Trie.verifyProof(proof.storageHash, util.keccak256(key_score), proof.storageProof[2].proof, (err, value) => {
                if(err || !value.equals(util.rlp.encode(proof.storageProof[2].value))){
                    throw new Error('Error validating registration proof');
                }
            });
            // VALIDATING PACKAGE LAST VERSION HASH
            Trie.verifyProof(proof.storageHash, util.keccak256(key_last_version), proof.storageProof[3].proof, (err, value) => {
                if(err || !value.equals(util.rlp.encode(proof.storageProof[3].value))){
                    throw new Error('Error validating last version proof');
                }
            });

            // RETURN TO PYTHON
            console.log(proof['block-number'], proof.storageProof[1].value, proof.storageProof[3].value, proof.storageProof[2].value);
        });
    }
});
