#!/usr/bin/env python3
#
# smartwitness.py - Extension to include SmartWitness into Fdroid as proof of
# concept for research paper and artifact publication
# Copyright (C) 2021 Juan Guarnizo <juan_guarnizo@mymail.sutd.edu.sg>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import csv
import glob
import time
import yaml
import ntpath
import hashlib
import zipfile

from argparse import ArgumentParser
from argparse import REMAINDER

from jproperties import Properties

from web3 import Web3
from web3.middleware import geth_poa_middleware
from web3.middleware import construct_sign_and_send_raw_middleware

from . import _
from . import common
from .exception import FDroidException

SMARTWITNESS_CONFIG_FILE = "smartwitness.yml"

PROPERTIES_FILENAME = "smartwitness.properties"
TEMP_PROPERTIES_FILE = "/tmp/%s" % PROPERTIES_FILENAME


config = None
options = None

def main():
    global options, config

    # Parse command line...
    parser = ArgumentParser()
    common.setup_global_opts(parser)
    parser.add_argument("-rp", "--repacking", default=None, action="store_true",
                        help=_("Flag that indicates repacking is the action to perform"))
    parser.add_argument("-r", "--register", default=None, action="store_true",
                        help=_("Flag that indicates register is the action to perform"))
    parser.add_argument("-p", "--proof", default=None, action="store_true",
                        help=_("Flag that indicates extracting is the action to perform"))
    parser.add_argument("-rt", "--rate", default=None, action="store_true",
                        help=_("Flag that indicates extracting is the action to perform"))
    parser.add_argument("-pk", "--package", default="all",
                        help=_("Package to rate in the repository, 'all' for every package in the repository"))
    parser.add_argument("-s", "--score", default=0, type=int,
                        help=_("score to assigned to rated packages"))
    parser.add_argument("--cve", default="None",
                        help=_("Indicates any CVE assigned to this rating"))
    
    options = parser.parse_args()
    fdroiddir = os.getcwd()

    if not os.path.exists('%s/repo' % (fdroiddir)):
        raise FDroidException("repo folder not found.")

    if not os.path.exists('%s/%s' % (fdroiddir, SMARTWITNESS_CONFIG_FILE)):
        raise FDroidException("SmartWitness config file not found.")

    config = read_config_file('%s/%s' % (fdroiddir, SMARTWITNESS_CONFIG_FILE))
    if options.repacking:
        repacking(fdroiddir)
    
    if options.register:
        register(fdroiddir)
    
    if options.proof:
        extract_proof(fdroiddir)
    
    if options.rate:
        rate(fdroiddir)

def repacking(fdroiddir):
    packages = get_all_packages(fdroiddir)

    props = create_properties_file()
    
    counter = 0
    total = len(packages)
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        unaligned = "/tmp/%s" % (filename.replace(extension, "_unaligned" + extension))
        aligned = "/tmp/%s" % (filename.replace(extension, "_aligned" + extension))

        zin = zipfile.ZipFile(package, 'r')
        zout = zipfile.ZipFile(unaligned, 'w')

        for item in zin.infolist():
            buffer = zin.read(item.filename)
            if not (item.filename.startswith('META-INF/') or item.filename.startswith(PROPERTIES_FILENAME)):
                zout.writestr(item, buffer)
        zout.writestr(PROPERTIES_FILENAME, props)
        
        zout.close()
        zin.close()

        from subprocess import Popen, PIPE

        p = Popen(['zipalign', '4', unaligned, aligned], stdout=PIPE, stderr=PIPE)
        output, error = p.communicate()
        if p.returncode != 0: 
            print("failed %d %s %s" % (p.returncode, output, error))
            continue

        # Missing Signature
        p = Popen(['apksigner', 'sign', '--v1-signing-enabled', 
                   '--ks', config["keystore"]["store"],
                   '--ks-pass', config["keystore"]["password"],
                   '--ks-key-alias', config["keystore"]["alias"],
                   '--out', package,
                   aligned], 
                   stdout=PIPE, stderr=PIPE)
        output, error = p.communicate()
        if p.returncode != 0: 
            print("failed %d %s %s" % (p.returncode, output, error))
            continue

        os.remove(unaligned)
        os.remove(aligned)

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))

def register(fdroiddir):
    w3 = connet()
    witness = w3.eth.contract(address=config['smartwitness']['witness-addr'], abi=config['smartwitness']['abi'])

    packages = get_all_packages(fdroiddir)

    counter = 0
    total = len(packages)
    receipts = {}
    gasPrice = w3.eth.gasPrice
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        filehash = sha256sum(package)

        txhash = witness.functions.registerPackage(filename.replace(extension, ""), filehash).transact({'gasPrice': gasPrice, 'gas': 100000})
        w3.eth.wait_for_transaction_receipt(txhash)
        receipt = w3.eth.get_transaction_receipt(txhash)
        receipts[package] = receipt

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))

    with open('register.csv', 'w') as csvfile:
        fieldnames = ['package', 'txhash', 'gasprice', 'gasused']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for key in receipts:
            receipt = receipts[key]
            writer.writerow({'package': key, 'txhash': receipt['transactionHash'].hex(), 'gasprice': gasPrice, 'gasused': receipt['gasUsed']})

def extract_proof(fdroiddir):
    w3 = connet()
    witness = w3.eth.contract(address=config['smartwitness']['witness-addr'], abi=config['smartwitness']['abi'])
    block = w3.eth.get_block('latest')

    packages = get_all_packages(fdroiddir)

    counter = 0
    total = len(packages)

    start = time.time()
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        filehash = sha256sum(package)

        # get data to calculate storage location
        name = w3.toHex(text=filename.replace(extension, ""))

        mapping_index = ('0' * 63) + '2'
        last_index = w3.sha3(hexstr=name + mapping_index)
        score_index = w3.sha3(hexstr=('0x' + filehash.hex() + increase_mapping_key(last_index)))
        proof = w3.eth.get_proof(witness.address, [0, 1, last_index, score_index], block.number)

        data = {
            "block": block,
            "proof": proof
        }
        with open(package.replace(extension, '.json'), 'w') as f:
            f.write(w3.toJSON(data))

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))
    end = time.time()
    print("Time:", end - start)

def rate(fdroiddir):
    
    w3 = connet()
    witness = w3.eth.contract(address=config['smartwitness']['witness-addr'], abi=config['smartwitness']['abi'])

    if options.package != "all" and not os.path.exists('%s/%s' % (fdroiddir, options.package)):
        raise FDroidException('%s/%s not found' % (fdroiddir, options.package))

    packages = [options.package]
    if options.package == "all":
        packages = get_all_packages(fdroiddir)

    counter = 0
    total = len(packages)
    receipts = {}
    gasPrice = w3.eth.gasPrice
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        filehash = sha256sum(package)

        txhash = witness.functions.ratePackage(filename.replace(extension, ""), filehash, options.score, options.cve).transact({'gasPrice': gasPrice, 'gas': 100000})
        w3.eth.wait_for_transaction_receipt(txhash)
        receipt = w3.eth.get_transaction_receipt(txhash)
        receipts[package] = receipt

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))

    with open('rating.csv', 'w') as csvfile:
        fieldnames = ['package', 'txhash', 'gasprice', 'gasused']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for key in receipts:
            receipt = receipts[key]
            writer.writerow({'package': key, 'txhash': receipt['transactionHash'].hex(), 'gasprice': gasPrice, 'gasused': receipt['gasUsed']})


def create_properties_file():
    p = Properties()
    p["dev-addr"] = config["smartwitness"]["dev-addr"]
    p["ca-addr"] = config["smartwitness"]["ca-addr"]
    p["ca-nonce"] = str(config["smartwitness"]["ca-nonce"])

    with open(TEMP_PROPERTIES_FILE, "wb") as f:
        p.store(f, encoding="utf-8")

    return open(TEMP_PROPERTIES_FILE, "r").read()

def get_all_packages(directory):
    if not directory.endswith("/"):
        fdroiddir = directory + "/"
    
    packages = []
    packages.extend(glob.glob(fdroiddir + "repo/*.apk"))
    packages.extend(glob.glob(fdroiddir + "repo/*.APK"))

    return packages

def read_config_file(path):
    with open(path) as f:
        return yaml.safe_load(f)

def connet():
    global options, config

    w3 = Web3(Web3.HTTPProvider(config["blockchain"]["rpc-server"]))
    w3.middleware_onion.inject(geth_poa_middleware, layer=0) 
    
    w3.eth.account.enable_unaudited_hdwallet_features()
    acc = w3.eth.account.from_mnemonic(config["blockchain"]["mnemonic"])
    w3.middleware_onion.add(construct_sign_and_send_raw_middleware(acc))
    w3.eth.default_account = acc.address

    return w3

def sha256sum(filename):
    h  = hashlib.sha256()
    b  = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda : f.readinto(mv), 0):
            h.update(mv[:n])
    return h.digest()

def increase_mapping_key(_hex, to_hex=False):
    n = int(_hex.hex(), 16)
    n += 1
    h = n.to_bytes(32, 'big').hex()
    return (('0x' if to_hex else '') + h)