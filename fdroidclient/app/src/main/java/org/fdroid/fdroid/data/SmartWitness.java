package org.fdroid.fdroid.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SmartWitness extends ValueObject {

    @JsonIgnore
    protected long id;
    @JsonIgnore
    public int activated;
    @JsonIgnore
    public int minScore;
    @JsonIgnore
    public int minReviews;
    @JsonIgnore
    public int maxProofAge;
    @JsonIgnore
    public int onlyLast;
    @JsonIgnore
    public String trustedCA;


    public SmartWitness() {
    }

    public SmartWitness(Cursor cursor) {
        checkCursorPosition(cursor);
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            switch (cursor.getColumnName(i)) {
                case Schema.SmartWitnessTable.Cols._ID:
                    id = cursor.getInt(i);
                    break;
                case Schema.SmartWitnessTable.Cols.ACTIVATED:
                    activated = cursor.getInt(i);
                    break;
                case Schema.SmartWitnessTable.Cols.MIN_SCORE:
                    minScore = cursor.getInt(i);
                    break;
                case Schema.SmartWitnessTable.Cols.MIN_REVIEWS:
                    minReviews = cursor.getInt(i);
                    break;
                case Schema.SmartWitnessTable.Cols.MAX_PROOF_AGE:
                    maxProofAge = cursor.getInt(i);
                    break;
                case Schema.SmartWitnessTable.Cols.ONLY_LAST:
                    onlyLast = cursor.getInt(i);
                    break;
                case Schema.SmartWitnessTable.Cols.TRUSTED_CA:
                    trustedCA = cursor.getString(i);
                    break;
            }
        }
    }

    public void setValues(ContentValues values) {
        if (values.containsKey(Schema.SmartWitnessTable.Cols._ID)) {
            id = toInt(values.getAsInteger(Schema.SmartWitnessTable.Cols._ID));
        }

        if (values.containsKey(Schema.SmartWitnessTable.Cols.ACTIVATED)) {
            activated = toInt(values.getAsInteger(Schema.SmartWitnessTable.Cols.ACTIVATED));
        }

        if (values.containsKey(Schema.SmartWitnessTable.Cols.MIN_SCORE)) {
            minScore = toInt(values.getAsInteger(Schema.SmartWitnessTable.Cols.MIN_SCORE));
        }

        if (values.containsKey(Schema.SmartWitnessTable.Cols.MIN_REVIEWS)) {
            minReviews = toInt(values.getAsInteger(Schema.SmartWitnessTable.Cols.MIN_REVIEWS));
        }

        if (values.containsKey(Schema.SmartWitnessTable.Cols.MAX_PROOF_AGE)) {
            maxProofAge = toInt(values.getAsInteger(Schema.SmartWitnessTable.Cols.MAX_PROOF_AGE));
        }

        if (values.containsKey(Schema.SmartWitnessTable.Cols.ONLY_LAST)) {
            onlyLast = toInt(values.getAsInteger(Schema.SmartWitnessTable.Cols.ONLY_LAST));
        }

        if (values.containsKey(Schema.SmartWitnessTable.Cols.TRUSTED_CA)) {
            trustedCA = values.getAsString(Schema.SmartWitnessTable.Cols.TRUSTED_CA);
        }
    }

    private static int toInt(Integer value) {
        if (value == null) {
            return 0;
        }
        return value;
    }

    @Override
    public String toString() {
        return "SmartWitness{" +
                "id=" + id +
                ", activated=" + activated +
                ", minScore=" + minScore +
                ", minReviews=" + minReviews +
                ", maxProofAge=" + maxProofAge +
                ", onlyLast=" + onlyLast +
                ", trustedCA=" + trustedCA +
                '}';
    }
}
