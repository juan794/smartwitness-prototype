package org.fdroid.fdroid.installer;

import android.content.Context;
import android.net.Uri;

import org.fdroid.fdroid.Hasher;
import org.fdroid.fdroid.data.SanitizedFile;
import org.fdroid.fdroid.data.SmartWitness;
import org.fdroid.fdroid.data.SmartWitnessProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.web3j.crypto.Hash;
import org.web3j.rlp.RlpDecoder;
import org.web3j.rlp.RlpEncoder;
import org.web3j.rlp.RlpList;
import org.web3j.rlp.RlpString;
import org.web3j.rlp.RlpType;
import org.web3j.utils.Numeric;
import org.web3j.utils.Strings;

public class SmartWitnessVerifier {

    // New Functionality to verify smartwitness data
    public static String getAndCheckProof(Context context, Uri uri, File apkFile, int timeout) {
        SanitizedFile localFile = new SanitizedFile(context.getCacheDir(), "proof.json");

        try {
            SmartWitness smartWitness = SmartWitnessProvider.Helper.get(context);
            if (smartWitness == null) {
                return "Policy verification failed";
            } else if (smartWitness.activated == 0) {
                return "";
            }

            ZipFile zip = new ZipFile(apkFile);
            ZipEntry entry = zip.getEntry("smartwitness.properties");
            if (entry == null) {
                return "no metadata";
            }

            InputStream stream = zip.getInputStream(entry);
            Properties props = new Properties();
            props.load(stream);
            if (!props.containsKey("ca-addr") || !props.containsKey("ca-nonce")) {
                return "no correct metadata";
            }

            JSONObject blockLastest = getBlockByNumber("latest", timeout);
            JSONObject jsonObject = makeRequestForJSON(
                uri.toString().replace(".apk", ".json"),
                "",
                timeout
            );

            if (!jsonObject.has("block") || !jsonObject.has("proof") ||
                    !blockLastest.has("number")) {
                return "Wrong JSON " + uri.toString().replace(".apk", ".json");
            }

            JSONObject block = jsonObject.getJSONObject("block");
            String blockNumberStr = block.getString("number");
            long proofBlockNumber;
            if (blockNumberStr.startsWith("0x")) {
                proofBlockNumber = Numeric.toBigInt(blockNumberStr).longValue();
            } else {
                proofBlockNumber = Long.valueOf(blockNumberStr);
            }
            block = getBlockByNumber("0x" + Long.toString(proofBlockNumber, 16), timeout);
            if (!block.has("stateRoot")) {
                return block.toString();
            }

            JSONObject proof = jsonObject.getJSONObject("proof");

            String address = proof.getString("address");
            String filename = apkFile.toPath().getFileName().toString();
            String extesion = filename.substring(filename.lastIndexOf("."));
            String filehash = new Hasher("SHA-256", apkFile).getHash();
            String name = Numeric.toHexString(filename.replace(extesion, "").getBytes());

            String lastIndex = Hash.sha3(name + Strings.repeat('0', 63) + "2");
            BigInteger bn = Numeric.toBigInt(lastIndex).add(BigInteger.ONE);
            String scoreIndex = Hash.sha3(filehash + Numeric.toHexStringNoPrefix(Numeric.toBytesPadded(bn, 32)));

            // Get all data from JSON
            byte[] stateRoot = Numeric.hexStringToByteArray(block.getString("stateRoot"));
            List<byte[]> accProof = formatProofNodes(proof.getJSONArray("accountProof"));
            byte[] accKey = Hash.sha3(Numeric.hexStringToByteArray(address));

            // Proper Acc RLP encoding
            BigInteger nonce = new BigInteger(proof.get("nonce").toString());
            BigInteger balance = new BigInteger(proof.get("balance").toString());
            // fixed length 32, allow empty
            byte[] storageHash = Numeric.hexStringToByteArray(proof.getString("storageHash"));
            // fixed length 32
            byte[] codeHash = Numeric.hexStringToByteArray(proof.getString("codeHash"));

            List<RlpType> acc = new ArrayList<>();
            acc.add(RlpString.create(nonce));
            acc.add(RlpString.create(balance));
            acc.add(RlpString.create(storageHash));
            acc.add(RlpString.create(codeHash));

            RlpList rlpList = new RlpList(acc);
            byte[] accEncoded = RlpEncoder.encode(rlpList);

            boolean accVef = _verify(stateRoot, Numeric.toHexString(accKey).substring(2), accEncoded, accProof, 0, 0);
            if (!accVef) {
                return "Account verification failed";
            }

            List<RlpType> propsListForRLP = new ArrayList<>();
            propsListForRLP.add(RlpString.create(Numeric.hexStringToByteArray(props.getProperty("ca-addr").toLowerCase())));
            propsListForRLP.add(RlpString.create(Integer.parseInt(props.getProperty("ca-nonce"))));

            RlpList encodedPropsRLP = new RlpList(propsListForRLP);
            byte[] encodedProps = RlpEncoder.encode(encodedPropsRLP);
            byte[] hashProps = Hash.sha3(encodedProps);
            String calculatedAddress = Numeric.toHexString(hashProps, hashProps.length - 20, 20, true);

            if (!calculatedAddress.equals(address.toLowerCase()) || !smartWitness.trustedCA.equals(props.getProperty("ca-addr"))) {
                return "Trusted CA verification failed " + smartWitness.trustedCA + " " + props.getProperty("ca-addr") + " " + address + " " + calculatedAddress;
            }

            String latestBlockNumberStr = blockLastest.getString("number");
            long latestBlockNumber;
            if (latestBlockNumberStr.startsWith("0x")) {
                latestBlockNumber = Numeric.toBigInt(latestBlockNumberStr).longValue();
            } else {
                latestBlockNumber = Long.valueOf(latestBlockNumberStr);
            }
            long diff = latestBlockNumber - proofBlockNumber;
            if (smartWitness.maxProofAge < diff) {
                return "Policy verification failed: proof age";
            }

            JSONArray storageProofArray = proof.getJSONArray("storageProof");
            for (int i = 0; i < storageProofArray.length(); i++) {
                byte[] storageKey = Numeric.hexStringToByteArray(storageProofArray.getJSONObject(i).getString("key"));
                byte[] storageVal = Numeric.hexStringToByteArray(storageProofArray.getJSONObject(i).getString("value"));

                if (storageKey.length < 32) {
                    storageKey = leftPaddingZeros(storageKey, 32);
                }

                byte[] valEncoded = new byte[]{};
                if (storageVal.length > 1 || storageVal[0] != 0 ) {
                    valEncoded = RlpEncoder.encode(RlpString.create(storageVal));
                }

                List<byte[]> storageProof = formatProofNodes(storageProofArray.getJSONObject(i).getJSONArray("proof"));
                boolean storageVef = _verify(storageHash, Numeric.toHexString(Hash.sha3(storageKey)).substring(2), valEncoded, storageProof, 0, 0);
                if (!storageVef) {
                    return "Storage verification failed";
                }

                BigInteger key = new BigInteger(storageKey);
                if (i == 0 && !Numeric.toHexString(storageVal).equals(props.get("dev-addr").toString().toLowerCase())) {
                    return "Ownership verification failed";
                }

                if (i == 1) {
                    Date date = new Date(new BigInteger(storageVal).longValue());
                    if (date.after(new Date())) {
                        return "Expiration verification failed";
                    }
                }

                if (i == 2 && smartWitness.onlyLast > 0 &&
                        (!Numeric.toHexString(storageKey).equals(lastIndex) ||
                        !Numeric.toHexStringNoPrefix(storageVal).equals(filehash))) {
                    return "Policy verification failed: last version " + Numeric.toHexStringNoPrefix(storageVal) + " " + filehash;
                }

                if (i == 3) {
                    if (!Numeric.toHexString(storageKey).equals(scoreIndex))
                        return "Policy verification failed";
                    byte counter = 0;
                    byte score = 100;
                    if (storageVal.length == 1) {
                        score = storageVal[0];
                    } else {
                        counter = storageVal[0];
                        score =  storageVal[1];
                    }
                    if (score < smartWitness.minScore)
                        return "Policy verification failed: score";
                    if (counter < smartWitness.minReviews)
                        return "Policy verification failed: reviews";
                }
            }
        } catch (IOException | JSONException | NoSuchAlgorithmException e) {
            return "Error:" + e.getMessage();
        }
        return "";
    }

    public static boolean _verify(byte[] root, String key, byte[] value, List<byte[]> proof, int idxProof, int idxKey) {
        byte[] node = proof.get(idxProof);
        RlpList dec = RlpDecoder.decode(node);
        if (dec.getValues().size() == 1) {
            dec = (RlpList) dec.getValues().get(0);
        }

        if (!Arrays.equals(Hash.sha3(node), root)) {
            return false;
        }

        if (idxKey > key.length()) {
            return false;
        }

        int decSize = dec.getValues().size();
        if (decSize == 17) {
            if (idxKey >= key.length()) {
                byte[] expectedValue = ((RlpString)dec.getValues().get(decSize - 1)).getBytes();
                if (Arrays.equals(expectedValue, value)) {
                    return true;
                }
            } else {
                byte[] newRoot = ((RlpString)dec.getValues().get(nibbleToNumber(key.substring(idxKey, idxKey+1)))).getBytes();
                if (newRoot.length > 0) {
                    return _verify(newRoot, key, value, proof, idxProof+1, idxKey+1);
                }
            }
        } else if (decSize == 2) {
            String prefixAndKey = Numeric.toHexString(((RlpString)dec.getValues().get(0)).getBytes()).substring(2);
            if (prefixAndKey.startsWith("2")) {
                String keyEnd = prefixAndKey.substring(2); // third string
                byte[] expectedValue = ((RlpString)dec.getValues().get(1)).getBytes();
                return (keyEnd.compareTo(key.substring(idxKey)) == 0 && Arrays.equals(expectedValue, value));
            } else if (prefixAndKey.startsWith("3")) {
                String keyEnd = prefixAndKey.substring(1); // second string
                byte[] expectedValue = ((RlpString)dec.getValues().get(1)).getBytes();
                return (keyEnd.compareTo(key.substring(idxKey)) == 0 && Arrays.equals(expectedValue, value));
            } else if (prefixAndKey.startsWith("0")) {
                String sharedNibbles = prefixAndKey.substring(2); // third string
                if (sharedNibbles.compareTo(key.substring(idxKey, idxKey + sharedNibbles.length())) == 0) {
                    byte[] newRoot = ((RlpString)dec.getValues().get(1)).getBytes();
                    return _verify(newRoot, key, value, proof, idxProof + 1, idxKey + sharedNibbles.length());
                }
            } else if (prefixAndKey.startsWith("1")) {
                String sharedNibbles = prefixAndKey.substring(2); // third string
                if (sharedNibbles.compareTo(key.substring(idxKey, idxKey + sharedNibbles.length())) == 0) {
                    byte[] newRoot = ((RlpString)dec.getValues().get(1)).getBytes();
                    return _verify(newRoot, key, value, proof, idxProof + 1, idxKey + sharedNibbles.length());
                }
            }
        }
        return (value.length == 0);
    }

    private static JSONObject getBlockByNumber(String number, int timeout) {
        String params = String.format(
                "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"eth_getBlockByNumber\", \"params\": [\"%s\", false]}",
                number
        );
        JSONObject obj = makeRequestForJSON(
                "https://polygon-mumbai.infura.io/v3/7de96f91f33f44baa68de823a71dca97",
                params,
                timeout
        );
        if (!obj.has("result")) {
            return obj;
        }
        try {
            return obj.getJSONObject("result");
        } catch (JSONException e) {
            return obj;
        }
    }

    public static JSONObject makeRequestForJSON(String uri, String params, int timeout) {
        try {
            URL url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            byte[] data = null;
            if (!params.isEmpty()) {
                data = params.getBytes(StandardCharsets.UTF_8);
            }

            connection.setConnectTimeout(timeout);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setUseCaches(false);
            if (data != null) {
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Length", Integer.toString(data.length));
                try(DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.write(data);
                }
            }
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return new JSONObject();
            }

            BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                result.append(line);
            }

            String jsonStr = result.toString();
            JSONObject jsonObject = new JSONObject(jsonStr);
            return jsonObject;
        } catch (IOException | JSONException e) {
            return new JSONObject();
        }
    }

    public static byte[] leftPaddingZeros(byte[] value, int size) {
        byte[] bs = new byte[size];
        if (size < value.length) {
            return null;
        }
        int startPos = size - value.length;
        System.arraycopy(value, 0, bs, startPos, value.length);

        return bs;
    }

    public static int nibbleToNumber(String str) {
        return Integer.parseInt(str, 16);
    }

    public static List<byte[]> formatProofNodes(JSONArray obj) {
        List<byte[]> ret = new ArrayList<>();
        try {
            for (int i = 0; i < obj.length(); i++) {
                String value = obj.getString(i);
                byte[] hex = Numeric.hexStringToByteArray(value);
                ret.add(hex);
            }
        } catch (Exception ex) {
        }
        return ret;
    }

}
