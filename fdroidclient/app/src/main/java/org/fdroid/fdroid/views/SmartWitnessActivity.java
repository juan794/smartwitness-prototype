package org.fdroid.fdroid.views;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.core.app.TaskStackBuilder;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.slider.Slider;

import org.fdroid.fdroid.FDroidApp;
import org.fdroid.fdroid.R;
import org.fdroid.fdroid.Utils;
import org.fdroid.fdroid.data.Schema;
import org.fdroid.fdroid.data.SmartWitness;
import org.fdroid.fdroid.data.SmartWitnessProvider;
import org.fdroid.fdroid.installer.SmartWitnessVerifier;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;

import java.io.IOException;

public class SmartWitnessActivity extends AppCompatActivity {

    private static final String TAG = "SmartWitnessActivity";

    private final static String DEFAULT_TRUSTED_CA = "0xd10c72a5F6a350d454D2234C965620C43Ce5bCfd";

    private final Context context = SmartWitnessActivity.this;
    private SmartWitness smartWitness;

    private MaterialToolbar toolbar;

    private EditText editTextTrustedCA;
    private Slider seekBarMinScore;
    private Slider seekBarMinReviews;
    private Slider seekBarMaxProofAge;
    private CheckBox switchOnlyLast;
    private CheckBox switchActivated;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FDroidApp fdroidApp = (FDroidApp) getApplication();
        fdroidApp.applyPureBlackBackgroundInDarkTheme(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.smartwitness_activity);

        smartWitness = SmartWitnessProvider.Helper.get(context);
        if (smartWitness == null) {
            ContentValues values = new ContentValues(4);
            values.put(Schema.SmartWitnessTable.Cols.TRUSTED_CA, DEFAULT_TRUSTED_CA);
            values.put(Schema.SmartWitnessTable.Cols.MIN_SCORE, 0);
            values.put(Schema.SmartWitnessTable.Cols.MIN_REVIEWS, 0);
            values.put(Schema.SmartWitnessTable.Cols.MAX_PROOF_AGE, 1000);
            values.put(Schema.SmartWitnessTable.Cols.ONLY_LAST, 0);

            SmartWitnessProvider.Helper.insert(context, values);
            smartWitness = SmartWitnessProvider.Helper.get(context);
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveBeforeClosing();
                Intent upIntent = NavUtils.getParentActivityIntent(SmartWitnessActivity.this);
                if (NavUtils.shouldUpRecreateTask(SmartWitnessActivity.this, upIntent) || isTaskRoot()) {
                    TaskStackBuilder.create(SmartWitnessActivity.this).addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    NavUtils.navigateUpTo(SmartWitnessActivity.this, upIntent);
                }
            }
        });

        editTextTrustedCA = (EditText) findViewById(R.id.trusted_ca);
        seekBarMinScore = (Slider) findViewById(R.id.min_security_score);
        seekBarMinReviews = (Slider) findViewById(R.id.min_review_counter);
        seekBarMaxProofAge = (Slider) findViewById(R.id.max_proof_age);
        switchOnlyLast = (CheckBox) findViewById(R.id.only_last_version);
        switchActivated = (CheckBox) findViewById(R.id.activated);

        editTextTrustedCA.setText(smartWitness.trustedCA);
        seekBarMinScore.setValue(smartWitness.minScore);
        seekBarMinReviews.setValue(smartWitness.minReviews);
        seekBarMaxProofAge.setValue(smartWitness.maxProofAge);
        switchOnlyLast.setChecked(smartWitness.onlyLast == 1);
        switchActivated.setChecked(smartWitness.activated == 1);

    }

    private void saveBeforeClosing() {
        ContentValues values = new ContentValues(4);
        values.put(Schema.SmartWitnessTable.Cols.TRUSTED_CA, editTextTrustedCA.getText().toString());
        values.put(Schema.SmartWitnessTable.Cols.MIN_SCORE, Math.round(seekBarMinScore.getValue()));
        values.put(Schema.SmartWitnessTable.Cols.MIN_REVIEWS, Math.round(seekBarMinReviews.getValue()));
        values.put(Schema.SmartWitnessTable.Cols.MAX_PROOF_AGE, Math.round(seekBarMaxProofAge.getValue()));
        values.put(Schema.SmartWitnessTable.Cols.ONLY_LAST, switchOnlyLast.isChecked() ? 1 : 0);
        values.put(Schema.SmartWitnessTable.Cols.ACTIVATED, switchActivated.isChecked() ? 1 : 0);

        int nrows = SmartWitnessProvider.Helper.update(context, smartWitness, values);
        if (nrows > 0) {
            smartWitness.setValues(values);
        }
    }
}
