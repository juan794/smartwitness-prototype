package org.fdroid.fdroid.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import org.fdroid.fdroid.BuildConfig;

import androidx.annotation.NonNull;

import org.fdroid.fdroid.Utils;

public class SmartWitnessProvider extends FDroidProvider {

    private static final String TAG = "SmartWitnessProvider";

    private static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".data";
    private static final String PROVIDER_NAME = "SmartWitnessProvider";

    public static final class Helper {

        private static final String TAG = "SmartWitnessProvider.Helper";

        private Helper() {
        }

        public static SmartWitness get(Context context) {
            ContentResolver resolver = context.getContentResolver();
            Uri uri = SmartWitnessProvider.getContentUri();
            Cursor cursor = resolver.query(uri, Schema.SmartWitnessTable.Cols.ALL, null, null, null);
            if (cursor == null || cursor.getCount() < 1) {
                return null;
            }
            return cursorToSmartWitness(cursor);
        }

        public static Uri insert(Context context, ContentValues values) {
            ContentResolver resolver = context.getContentResolver();
            Uri uri = SmartWitnessProvider.getContentUri();
            return resolver.insert(uri, values);
        }

        public static int update(Context context, SmartWitness smartWitness, ContentValues values) {
            ContentResolver resolver = context.getContentResolver();
            Uri uri = SmartWitnessProvider.getContentUri();
            final String[] args = {Long.toString(smartWitness.id)};
            return resolver.update(uri, values, Schema.SmartWitnessTable.Cols._ID + " = ?", args);
        }

        private static SmartWitness cursorToSmartWitness(Cursor cursor) {
            SmartWitness smartWitness = null;
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    smartWitness = new SmartWitness(cursor);
                }
                cursor.close();
            }
            return smartWitness;
        }

    }

    public static Uri getContentUri() {
        return Uri.parse("content://" + AUTHORITY + "." + PROVIDER_NAME);
    }

    @Override
    protected String getTableName() {
        return Schema.SmartWitnessTable.NAME;
    }

    @Override
    protected String getProviderName() {
        return "SmartWitnessProvider";
    }

    @Override
    protected UriMatcher getMatcher() {
        return null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection,
                        String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = db().query(getTableName(), projection,
                selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long id = db().insertOrThrow(getTableName(), null, values);
        Utils.debugLog(TAG, "Inserted smartwitness. Notifying provider change: '" + Long.toString(id) + "'.");
        //getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String where, String[] whereArgs) {
        return db().update(getTableName(), values, where, whereArgs);
    }

    @Override
    public int delete(@NonNull Uri uri, String where, String[] whereArgs) {
        QuerySelection selection = new QuerySelection(where, whereArgs);
        int rowsAffected = db().delete(getTableName(), selection.getSelection(), selection.getArgs());
        return rowsAffected;
    }


}