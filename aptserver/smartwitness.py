#!/usr/bin/env python3
#
# smartwitness.py - Extension to include SmartWitness into Fdroid as proof of
# concept for research paper and artifact publication
# Copyright (C) 2021 Juan Guarnizo <juan_guarnizo@mymail.sutd.edu.sg>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import csv
import glob
import time
import yaml
import ntpath
import hashlib
import subprocess

from argparse import ArgumentParser
from argparse import REMAINDER

from web3 import Web3
from web3.middleware import geth_poa_middleware
from web3.middleware import construct_sign_and_send_raw_middleware

SMARTWITNESS_CONFIG_FILE = "smartwitness.yml"

config = None
options = None

def main():
    global options, config

    parser = ArgumentParser()
    parser.add_argument("-rp", "--repacking", default=None, action="store_true",
                        help=("Flag that indicates repacking is the action to perform"))
    parser.add_argument("-r", "--register", default=None, action="store_true",
                        help=("Flag that indicates register is the action to perform"))
    parser.add_argument("-p", "--proof", default=None, action="store_true",
                        help=("Flag that indicates extracting is the action to perform"))
    parser.add_argument("-rt", "--rate", default=None, action="store_true",
                        help=("Flag that indicates extracting is the action to perform"))
    parser.add_argument("-pk", "--package", default="all",
                        help=("Package to rate in the repository, 'all' for every package in the repository"))
    parser.add_argument("-s", "--score", default=0, type=int,
                        help=("score to assigned to rated packages"))
    parser.add_argument("--cve", default="None",
                        help=("Indicates any CVE assigned to this rating"))
    
    options = parser.parse_args()
    repodir = os.getcwd()

    if not os.path.exists('%s/%s' % (repodir, SMARTWITNESS_CONFIG_FILE)):
        raise Exception("SmartWitness config file not found.")

    config = read_config_file('%s/%s' % (repodir, SMARTWITNESS_CONFIG_FILE))
    if options.repacking:
        repacking(repodir)
    
    if options.register:
        register(repodir)
    
    if options.proof:
        extract_proof(repodir)
    
    if options.rate:
        rate(repodir)

def repacking(repodir):
    packages = get_all_packages(repodir)
    
    counter = 0
    total = len(packages)
    print("%d processed out of %d" % (counter, total))
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)

        tempfolder = "/tmp/%s" % (filename.replace(extension, ""))
        original = "%s/%s" % (tempfolder, "DEBIAN/control")
        new = "%s/%s" % (tempfolder, "DEBIAN/control.new")

        subprocess.call(["dpkg-deb", "-R", package, tempfolder])

        with open(new, "w") as g:
            with open(original, "r") as f:
                for line in f:
                    if line.startswith("Dev-PK:") or line.startswith("CA:") or line.startswith("CA-Nonce:"):
                        continue
                    if line.startswith("Description:"):
                        g.write("Dev-Addr: " + config["smartwitness"]["dev-addr"] + "\n")
                        g.write("CA: " + config["smartwitness"]["ca-addr"] + "\n")
                        g.write("CA-Nonce: " + str(config["smartwitness"]["ca-nonce"]) + "\n")
                    g.write(line)
        subprocess.call(["rm", original])
        subprocess.call(["mv", new, original])
        subprocess.call(["dpkg-deb", "-b", tempfolder, package])
        subprocess.call(["rm", "-rf", tempfolder])

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))

def register(repodir):
    w3 = connet()
    witness = w3.eth.contract(address=config['smartwitness']['witness-addr'], abi=config['smartwitness']['abi'])

    packages = get_all_packages(repodir)

    counter = 0
    total = len(packages)
    receipts = {}
    gasPrice = w3.eth.gasPrice
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        filehash = sha256sum(package)

        txhash = witness.functions.registerPackage(filename.replace(extension, ""), filehash).transact({'gasPrice': gasPrice, 'gas': 100000})
        w3.eth.wait_for_transaction_receipt(txhash)
        receipt = w3.eth.get_transaction_receipt(txhash)
        receipts[package] = receipt

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))

    with open('register.csv', 'w') as csvfile:
        fieldnames = ['package', 'txhash', 'gasprice', 'gasused']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for key in receipts:
            receipt = receipts[key]
            writer.writerow({'package': key, 'txhash': receipt['transactionHash'].hex(), 'gasprice': gasPrice, 'gasused': receipt['gasUsed']})

def extract_proof(repodir):
    w3 = connet()
    witness = w3.eth.contract(address=config['smartwitness']['witness-addr'], abi=config['smartwitness']['abi'])
    block = w3.eth.get_block('latest')

    packages = get_all_packages(repodir)

    counter = 0
    total = len(packages)

    start = time.time()
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        filehash = sha256sum(package)

        # get data to calculate storage location
        name = w3.toHex(text=filename.replace(extension, ""))

        mapping_index = ('0' * 63) + '2'
        last_index = w3.sha3(hexstr=name + mapping_index)
        score_index = w3.sha3(hexstr=('0x' + filehash.hex() + increase_mapping_key(last_index)))
        proof = w3.eth.get_proof(witness.address, [0, 1, last_index, score_index], block.number)

        data = {
            "block": block,
            "proof": proof
        }
        with open(package.replace(extension, '.json'), 'w') as f:
            f.write(w3.toJSON(data))

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))
    end = time.time()
    print("Time:", end - start)

def rate(repodir):
    
    w3 = connet()
    witness = w3.eth.contract(address=config['smartwitness']['witness-addr'], abi=config['smartwitness']['abi'])

    if options.package != "all" and not os.path.exists('%s/%s' % (repodir, options.package)):
        raise Exception('%s/%s not found' % (repodir, options.package))

    packages = [options.package]
    if options.package == "all":
        packages = get_all_packages(repodir)

    counter = 0
    total = len(packages)
    receipts = {}
    gasPrice = w3.eth.gasPrice
    for package in packages:
        counter += 1
        _, filename = ntpath.split(package)
        _, extension = ntpath.splitext(package)
        filehash = sha256sum(package)

        txhash = witness.functions.ratePackage(filename.replace(extension, ""), filehash, options.score, options.cve).transact({'gasPrice': gasPrice, 'gas': 100000})
        w3.eth.wait_for_transaction_receipt(txhash)
        receipt = w3.eth.get_transaction_receipt(txhash)
        receipts[package] = receipt

        if (counter % 100) == 0 or counter == total:
            print("%d processed out of %d" % (counter, total))

    with open('rating.csv', 'w') as csvfile:
        fieldnames = ['package', 'txhash', 'gasprice', 'gasused']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for key in receipts:
            receipt = receipts[key]
            writer.writerow({'package': key, 'txhash': receipt['transactionHash'].hex(), 'gasprice': gasPrice, 'gasused': receipt['gasUsed']})

def get_all_packages(directory):
    repodir = None
    if not directory.endswith("/"):
        repodir = directory + "/"
    
    packages = []
    packages.extend(glob.glob(repodir + "**/*.deb", recursive=True))
    packages.extend(glob.glob(repodir + "**/*.DEB", recursive=True))

    return packages

def read_config_file(path):
    with open(path) as f:
        return yaml.safe_load(f)

def connet():
    global options, config

    w3 = Web3(Web3.HTTPProvider(config["blockchain"]["rpc-server"]))
    w3.middleware_onion.inject(geth_poa_middleware, layer=0) 
    
    w3.eth.account.enable_unaudited_hdwallet_features()
    acc = w3.eth.account.from_mnemonic(config["blockchain"]["mnemonic"])
    w3.middleware_onion.add(construct_sign_and_send_raw_middleware(acc))
    w3.eth.default_account = acc.address

    return w3

def sha256sum(filename):
    h  = hashlib.sha256()
    b  = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda : f.readinto(mv), 0):
            h.update(mv[:n])
    return h.digest()

def increase_mapping_key(_hex, to_hex=False):
    n = int(_hex.hex(), 16)
    n += 1
    h = n.to_bytes(32, 'big').hex()
    return (('0x' if to_hex else '') + h)

if __name__ == '__main__':
    main()